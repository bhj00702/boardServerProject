//===============================================================
// 모듈 설정
//===============================================================
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const session = require('express-session');
const http = require('http');
// const fs = require('fs');

//===============================================================
// express 설정
//===============================================================
const app = express();
const router = require('./module/router/router');

// view engine setup
app.set('views', path.join(__dirname, './public'));
app.set('view engine', 'html');
app.engine('html', ejs.renderFile);

app.use(bodyParser.json({ limit : "50mb" }));
app.use(bodyParser.urlencoded({ limit:"50mb",extended: false }));
app.use(cookieParser());

//세션설정
app.use(
    session({
        key : 'new_session_secret',
            secret : '9VZrrn0A5gSXQQ3gWbNoAqJVAvxBsw',
        cookie: {
            maxAge: 1000 * 60 * 60 * 24 * 7, // 쿠키 유효기간 7일
        },
        //store: sessionStore,
        //store: Redis.getInstance().getRedisStoreAdmin(),
        resave: false,
        rolling: true, // cookie 값을 요청시마다 받아감 마지막 요청 후 7일 경과되었을때 session expired
        saveUninitialized: true,
    })
);

//===============================================================
// express 라우팅
//===============================================================
app.use('/', router);

//===============================================================
// 리소스 검색
//===============================================================
// app.use(function (req, res, next) {
//     let requestPath = decodeURI(path.join(__dirname, './public', req.path));
//     if (fs.existsSync(requestPath)) {
//         //console.log(req.path+" >>> "+requestPath+" >>> O" );
//         res.sendFile(requestPath);
//         return;
//     } else {
//        //마지막으로 못찾았을 때는 uploads 폴더에서 다시한번 탐색
//         requestPath = decodeURI(path.join(__dirname, './uploads', req.path));
//         if (fs.existsSync(requestPath)) {
//             res.sendFile(requestPath);
//             return;
//         } else {
//             console.log(req.path + ' >>> ' + requestPath + ' >>> X');
//             //res.status(404).send('404 not found');
//         res.render('404.html');
//         }
//     }
// });

app.use(function (err, req, res, next) {
    console.log(err);
});

//===============================================================
// 서버 생성
//===============================================================
http.createServer(app).listen(8080, function ()
{
    console.log("✅ express 서버가 실행됨");
});