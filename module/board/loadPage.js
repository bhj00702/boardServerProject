const bodyParser = require('body-parser');
const db = require('../dataBase/dataBaseBoard');

function loadPage(req, res, callback)
{
    var paramidx = req.body.idx || req.query.idx;

    function getPageCallback(page)
    {
        console.log(page);
        if (page != undefined)
        {
            console.log("로딩 성공");
            callback(page, true);
            db.uppdateView(paramidx, page.view + 1);
        }
        else
        {
            console.log("로딩 실패");
            callback(page, false);
        }
    }
    db.dbfindidx(paramidx, getPageCallback);
}   

module.exports = loadPage;