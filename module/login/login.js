const bodyParser = require('body-parser');
const db = require('../dataBase/dataBase');

function login(req, res, router)
{
    var paramId = req.body.id || req.query.id;
    var paramPw = req.body.password || req.query.password;

    function loginCallback(user)
    {
        try 
        {
            if(paramPw == user["password"])
            {
                delete user.password;
                req.session.user = user;
                req.session.save(function ()
                {
                    res.redirect('/loginComplete');
                });
            }
            else
            {
                res.redirect('/loginFail');
            }
        } 
        catch (error) 
        {
            res.redirect('/loginFail');
        }
    }

    db.getUserInfo(paramId, loginCallback);
}

module.exports = login;