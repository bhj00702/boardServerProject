const express = require('express');
const session = require('express-session');
const write = require('../boardWriting/boardWrite');
const { getAllMemos, getMemo } = require('../dataBase/dataBaseBoard');
const login = require('../login/login');
const signUp = require('../signUp/signUp');
const loadPage = require('../board/loadPage');

const router = express.Router();

router.all('/404', async function (req, res, next) 
{
    res.render('404.html', {req: req});
});

router.all('/403', async function (req, res, next) 
{
    res.render('403.html', {req: req});
});

router.get('/', async function (req, res, next)
{
    getAllMemos(function(rows)
    {
        res.render("./board/index.html", {user:req.session.user, rows:rows});
    });
});

router.get('/login', async function (req, res, next) 
{
    if (req.session.user)
    {
        console.log("이미 로그인되어있음");
        res.render('./board/index.html');
    }
    res.render('./login/index.html');
});

router.post('/loginStart', async function (req, res, next)
{
    login(req, res, router);
});

router.get('/loginComplete', async function (req, res, next)
{
    res.render('./login/loginComplete.html');
});

router.get('/loginFail', async function (req, res, next)
{
    res.render('./login/loginFail.html');
});

router.get('/logout', async function (req, res, next)
{
    req.session.destroy();
    res.redirect('/');
});

router.get('/signUp', async function (req, res, next)
{
    res.render('./signUp/index.html');
});

router.post('/signUpStart', async function (req, res, next)
{
    signUp(req, res, router);
});

router.get('/signUpComplete', async function (req, res, next)
{
    res.render('./signUp/signUpComplete.html');
});

router.get('/signUpFail', async function (req, res, next)
{
    res.render('./signUp/signUpFail.html');
});

router.get('/write', async function (req, res, next)
{
    res.render('./boardWrite');
});

router.post('/writeStart', async function (req, res, next)
{
    write(req, res);
});

router.get('/writeComplete', async function (req, res, next)
{
    res.render('./boardWrite/loginComplete.html');
});

router.get('/writeFail', async function (req, res, next)
{
    res.render('./boardWrite/loginFail.html');
});

router.post("/load", async function (req, res, next)
{
    function loadPageCallback(page, type)
    {
        console.log("asdsadas");
        if (type == true)
        {
            console.log("a");
            res.redirect("/loadPage");
        }
        else if (type == false)
        {
            console.log("b");
            res.redirect("/loadFail");
        }
    };

    loadPage(req, res, loadPageCallback);
});

router.post("/loadPage", async function (req, res, next)
{
    loadPage(req, res, function(rows)
    {
        res.render("./pageOpen/page.html", {user:req.session.user, rows:rows});
    });
});

router.post("/loadFail", async function (req, res, next)
{
    console.log("a");
    res.render("./pageOpen/pageFail.html", {user:req.session.user, rows:rows});
});

module.exports = router;