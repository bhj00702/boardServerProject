const mysql = require('mysql2');

const connectSet =
{
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: '1234',
    database: 'board',
}

const connection = mysql.createConnection(connectSet);
connection.connect();

function dbInsert(title, userid, date, content) 
{
    var insertQuery = "INSERT INTO boardinfo(title, userid, date, content) VALUES ('" + title + "', '" + userid + "', '" + date + "', '" + content + "');";
    console.log(insertQuery);
    connection.query
    (
        insertQuery, (err, results, fields) => 
        {
            if (err) 
            {
                console.log(err);
            }
            console.log(results);
        }
    );
}

function dbSelect(column = "*")
{
    var selectQuery = "SELECT " + column + " FROM boardinfo;";
    connection.query
    (
         selectQuery, (err, results, fields) => 
        {
            if (err) 
            {
                console.log(err);
            }
            console.log(results);
            console.log(results[0]);
            return results;
        }
    );
    console.log(get["id"]);
}

function dbDelete(column = "*")
{
    var deleteQuery = "DELETE FROM boardinfo WHERE " + column + ";";

    connection.query
    (
        deleteQuery, (err, results, fields) =>
        {
            if (err)
            {
                console.log(err);
            }
            console.log(results);
        }
    );
}

function dbfindidx(idx, callback)
{
    var selectQuery = "SELECT * FROM boardinfo WHERE idx = '"+ idx +"';";
    try 
    {
        connection.query
        (
            selectQuery, (err, results, fields) =>
            {
                if (err)
                {
                    console.log(err);
                }
                callback(results[0]);
            }
        );
    } 
    catch (error) 
    {
        return null;
    }
    
}

function writing(idx, title, userid, date, content, sucessCallback)
{
    function writeCallback(user)
    {
        if(user == undefined)
        {
            dbInsert(title, userid, date, content)
            sucessCallback(user, true);
        }
        else
        {
            sucessCallback(user, false)
        }
        
    }

    dbfindidx(idx, writeCallback);
}

function getAllMemos(callback)
{
    connection.query('SELECT * FROM boardinfo ORDER BY idx DESC', function (err, rows, fields)
    {
        if (err)
        {
            console.log(err);
        }
        callback(rows);
    });
}

function uppdateView(targetidx, view)
{
    var updateQuery = "UPDATE board.boardinfo SET view = " + view + " WHERE idx = " + targetidx + ";";
    connection.query(updateQuery, function(err)
    {
        if (err)
        {
            console.log(err);
        }
        console.log("update complete");
    })
}

module.exports = 
{
    dbInsert,
    dbSelect,
    dbDelete,
    writing,
    dbfindidx,
    getAllMemos,
    uppdateView,
    // getMemo,
}
