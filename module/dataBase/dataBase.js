const mysql = require('mysql2');

const connectSet =
{
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: '1234',
    database: 'board'
}

const connection = mysql.createConnection(connectSet);
connection.connect();

function dbInsert(id, pw, userName, email, role) 
{
    var insertQuery = "INSERT INTO userinfo(id, password, name, email, role) VALUES ('" + id + "','" + pw + "','" + userName + "','" + email + "'," + role + ");";

    connection.query
    (
        insertQuery, (err, results, fields) => 
        {
            if (err) 
            {
                console.log(err);
            }
            console.log(results);
        }
    );
}

function dbSelect(column = "*")
{
    var selectQuery = "SELECT " + column + " FROM userinfo;";
    var get = null;
    connection.query
    (
         selectQuery, (err, results, fields) => 
        {
            if (err) 
            {
                console.log(err);
            }
            console.log(results);
            console.log(results[0]);
            return results;
        }
    );
    console.log(get["id"]);
}

function dbDelete(column = "*")
{
    var deleteQuery = "DELETE FROM userinfo WHERE " + column + ";";

    connection.query
    (
        deleteQuery, (err, results, fields) =>
        {
            if (err)
            {
                console.log(err);
            }
            console.log(results);
        }
    );
}

function dbFindId(column = "*")
{
    var selectQuery = "SELECT ID FROM userinfo WHERE ID = '"+ column +"';";
    connection.query
    (
        selectQuery, (err, results, fields) =>
        {
            if (err)
            {
                console.log(err);
            }

            try 
            {
                results = results[0]["ID"]
                results = 1;
            } 
            catch (error) 
            {
                results = 0;
            }
            console.log(results);
        }
    );
}

function getUserInfo(id = "*", sucessCallback)
{
    var selectQuery = "SELECT * FROM userinfo WHERE id = '"+ id +"';";
    try
    {
        connection.query
        (
            selectQuery, (err, results, fields) =>
            {
                if (err)
                {
                    console.log(err);
                }
                sucessCallback(results[0]);
            }
        );
    }
    catch (error)
    {
        return null;
    }
    
}

function signUpDb(id, pw, email, name, role, sucessCallback)
{
    function userInfocallback(user)
    {
        if (user == undefined)
        {
            dbInsert(id, pw, name, email, role)
            sucessCallback(user, true);
        }
        else
        {
            sucessCallback(user, false);
        }
    }
    getUserInfo(id, userInfocallback);

}

module.exports = 
{
    dbInsert,
    dbSelect,
    dbDelete,
    dbFindId,
    getUserInfo,
    signUpDb,
}
