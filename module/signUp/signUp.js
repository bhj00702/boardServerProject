const bodyParser = require('body-parser');
const db = require('../dataBase/dataBase');

function signUp(req, res, router)
{
    var paramId = req.body.id || req.query.id;
    var paramPw = req.body.password || req.query.password;
    var paramNa = req.body.name || req.query.name;
    var paramEm = req.body.email || req.query.email;

    function signUpCallback(user, bool)
    {
        if (bool == true)
        {
            res.redirect('/signUpComplete');
        }
        else
        {
            res.redirect('/signUpFail')
        }
    }

    db.signUpDb(paramId, paramPw, paramEm, paramNa, 0, signUpCallback);
}

module.exports = signUp;