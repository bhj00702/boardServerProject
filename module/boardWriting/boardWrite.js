const bodyParser = require('body-parser');
const db = require('../dataBase/dataBaseBoard');

function write(req, res, router)
{
    var userid = req.session.user.id;
    var paramidx = req.body.idx || req.query.idx;
    var paramTitle = req.body.title || req.query.title;
    var paramContent = req.body.content || req.query.content;
    var nowDate = new Date();
    var date = nowDate.getFullYear() + "/" + nowDate.getMonth() + "/" + nowDate.getDate();

    function writeCallback(user, bool)
    {
        if (bool == true)
        {
            res.redirect('/writeComplete');
        }
        else
        {
            res.redirect('/writeFail');
        }
    }

    db.writing(paramidx, paramTitle, userid, date, paramContent, writeCallback);
}

module.exports = write;